## coral-user Tiramisu TPB1.220310.029 8473704 release-keys
- Manufacturer: google
- Platform: msmnile
- Codename: coral
- Brand: google
- Flavor: coral-user
- Release Version: 12
- Id: TPB1.220310.029
- Incremental: 8473704
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:Tiramisu/TPB1.220310.029/8473704:user/release-keys
- OTA version: 
- Branch: coral-user-Tiramisu-TPB1.220310.029-8473704-release-keys
- Repo: google_coral_dump_26863


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
